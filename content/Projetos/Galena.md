+++
title = 'Galena'
date = 2024-10-08T15:06:59-03:00
draft = false
+++

![imgrobo](/images/galena.jpeg)
A Galena é um robô da equipe de robótica ThundeRatz da categoria dos 3kg, participa tanto da categoria autônoma quanto da radiocontrolada. A Galena surgiu em 2022, inspirada em projetos de sumôs japoneses. A galena é pensada como um projeto mais lento e pesado, com a força normal dos ímãs somando por volta de 120kg de força normal com o dohyo. Além disso, a Galena no modo radiocontrolado é equipada com a rampa tripla, que consiste em rampas laterais que começam levantadas, para respeitar as limitações de tamanho da categoria, e assim que é iniciado o round, um mecanismo ativado por uma solenoide desce essas rampas, criando efetivamente uma grande rampa que supera o limite de 200mm, respeitando a regra. Ela é o primeiro e único robô tri-rampa da america latina, e já conquistou diversos.