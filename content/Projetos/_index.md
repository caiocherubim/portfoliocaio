+++
title = 'Projetos'
date = 2024-10-08T15:07:09-03:00
draft = false
+++

Na equipe ThundeRatz, Caio trabalha principalmente com a parte mecânica dos robôs, mais especificamente dos da categoria de sumô.

Um robô de sumô é um tipo de robô projetado para participar de competições de sumô robótico, onde dois robôs competem entre si com o objetivo de empurrar o oponente para fora de uma arena circular, conhecida como dohyo. Inspirado na luta de sumô tradicional japonesa, o robô de sumô segue regras específicas da categoria, sendo que há um peso máximo permitido e suas dimensões devem estar dentro de um limite para garantir a conformidade com as normas da competição. Para a categoria do Mega-Sumô, o peso não pode exceder 3kg, e o comprimento e largura ambos têm um limite máximo de 200mm. Para a categoria do Mini-Sumô, esses valores são 500g e 100mm.

A estratégia de projeto mais comumente utilizada é o da utilização de uma rampa na frente do robô, que tem como objetivo entrar debaixo do oponenete e fazer com que ele perca contato das rodas com o dohyo, tornando fácil empurrá-lo para fora.

Os robôs podem ser controlados de duas formas: autonomamente, onde sensores e algoritmos programados controlam suas ações, ou por rádio controle, onde pilotos humanos manobram os robôs remotamente. Eles são equipados com diferentes tipos de sensores, como sensores infravermelhos para detectar o adversário e os limites da arena e sensores que detectam o oponente e realizam o alinhamento .

Na categoria dos 3kg, o dohyo é de metal. Aproveitando a propriedade ferromagnética do chão para melhorar o desempenho, os robôs de sumô utilizam ímãs para aumentar a normal em relação à superfície da arena. Isso permite uma maior tração, o que é essencial para a transferência de potência e evitar ser empurrado.

As competições de sumô robótico são populares globalmente, e a categoria de robôs de 3kg é uma das mais tradicionais, exigindo estratégias avançadas tanto no design mecânico quanto na programação e controle dos robôs.

Dentro da equipe, Caio participa e desenvolve 3 projetos de sumô:






