+++
title = 'Moai'
date = 2024-10-08T15:07:09-03:00
draft = false
+++
![imgrobo](/images/moai.jpeg)
O Moai é um robô da equipe de robótica ThundeRatz da categoria dos 3kg, que participa tanto da categoria autônoma quanto da radiocontrolada. O Moai foi projetado como o sucessor do robô Stonehenge e foi lançado no ano de 2014 como o primeiro sumô autônomo da equipe. O conceito por trás do Moai é ser um robô focado em velocidade, que é uma vantagem que pode ser muito bem explorada na categoria autônoma. O robô já passou por várias mudanças e reprojetos e atualmente encontra-se em sua quarta versão. Por ser mais focado em velocidade, têm menos ímãs que seu antecessor Stonehenge, tendo um peso aparente de 70kg quando colocado sobre o dohyo metálico.
