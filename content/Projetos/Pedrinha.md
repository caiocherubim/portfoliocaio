+++
title = 'Pedrinha'
date = 2024-10-08T15:07:09-03:00
draft = false
+++
![imgrobo](/images/pedrinha.jpeg)
O Pedrinha é um robô recente da equipe Thunderatz, estreiando na RCX 2024 e que participa da categoria Mini Sumô Radio Controlado. O projeto foi criado com o objetivo de incentivar os recém ingressantes da equipe, possibilitando  o desenvolvimento de autonomia, noções de gestão de projetos e o desenvolvimento de conhecimento sobre a caregoria.

Esse mini sumô foi inspirado em projetos anteriores, como, por exemplo: Rozeta, Ônix e Safira, e a sua característica principal é a sua fabricação baseada no reaproveitamento de componentes e materiais de outros projetos.

