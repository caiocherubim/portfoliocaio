# Caio Hashizume Cherubim

**Contato:**
- Telefone: (19) 97122-2004
- Email: caiocherubim@usp.br

---

## Formação Acadêmica

**Engenharia Mecatrônica**  
*Escola Politécnica da Universidade de São Paulo (USP)*  
*Início:* Fevereiro de 2022 | *Previsão de Conclusão:* Dezembro de 2026


---

## Experiência Acadêmica e Projetos

### ThundeRatz - Equipe de Robótica Competitiva  
**Membro da equipe de mecânica de robôs de sumô 3kg**  
*Maio de 2023 – Presente*

- Desenvolvimento de robôs de sumô autônomos e radiocontrolados para competições.
- Foco na integração mecânica e design de componentes para otimização de desempenho.
- Competição IRONCup 2024, 2º lugar na categoria Sumô 3kg Autônomo com a Galena, e 3º lugar na categoria Sumô 3kg RC com o Moai.
- Competição RSM Challenge Internacional 2024, 3º lugar na categoria Sumô 3kg RC com a Galena.
- Competição Robocore Experience 2024, 3º lugar na categoria Sumô 3kg Autônomo com a Galena.


---

## Habilidades Técnicas

- **Programação**: Python, MATLAB.
- **Ferramentas de CAD**: SolidWorks, AutoCAD.
- **Simulações**: Abaqus, ANSYS.
---

## Idiomas

- **Inglês**: Avançado
- **Português**: Nativo




