
## Portfólio de Caio Cherubim

Este é o portfólio de **Caio Cherubim**, onde compartilho minha trajetória acadêmica e projetos na área de **Engenharia Mecatrônica** e **Robótica**.

---

## O que você encontrará aqui?

- **Minha Biografia**: Conheça minha história e formação.
- **Meus Projetos**: Veja os projetos em que trabalhei, especialmente na área de robótica.
- **Meu Currículo**: Acesse o meu currículo completo para mais detalhes sobre minhas qualificações.

---

