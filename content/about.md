+++
title = 'Sobre'
date = 2024-10-08T14:35:34-03:00
draft = false
+++
![imgpessoa](/images/caiosite.jpeg)
Caio Hashizume Cherubim, nascido no dia 6 de Junho de 2004, é um jovem de 20 anos, natural de Vinhedo, onde passou sua infância e adolescência. Apaixonado por tecnologia e inovação, ele decidiu seguir carreira na área de engenharia mecatrônica. Atualmente, Caio está cursando o terceiro ano de Engenharia Mecatrônica na Escola Politécnica da Universidade de São Paulo (USP). Mudou-se para São Paulo para se dedicar aos estudos e tem se destacado em disciplinas que combinam robótica, automação e eletrônica. Buscando desenvolver seus conhecimentos na área, Caio faz parte da equipe de robótica competitiva ThundeRatz da USP, onde trabalha principalmente com a estrutura mecânica dos robôs da categoria de sumôs. 

Caio ao longo de sua vida também sempre teve muito interesse em esportes, lutando kungfu e praticando tênis na sua infância, além de jogar voleibol no ensino médio. Ao entrar na faculdade, participou dos treinos de remo na raia olímpica. Atualmente, tem interesse em corrida e é o que tem praticado recentemente.
